import $ from "jquery";
document.addEventListener("DOMContentLoaded", () => {
    $("a[href^='#'][rel~='smooth-scroll']").on('click', function (e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $($(e.target).attr('href')).offset().top
        });
    });
});
