module.exports = function(grunt){
    grunt.initConfig({
        exec: {
            publish: 'npm publish --access public'
        },
        webpack: {
            build: Object.assign(require('./webpack.config'), {watch: false})
        }
    });
    grunt.loadNpmTasks('grunt-exec');
    grunt.loadNpmTasks('grunt-webpack');
	grunt.registerTask('pub', ['exec:publish']);
};
